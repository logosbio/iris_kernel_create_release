/*
 * drivers\rtc\rtc-sun4i.c
 * (C) Copyright 2007-2011
 * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
 * huangxin <huangxin@allwinnertech.com>
 *
 * some simple description for this code
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 */
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/string.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/rtc.h>
#include <linux/bcd.h>
#include <linux/clk.h>
#include <linux/log2.h>
#include <linux/delay.h>

#include <plat/platform.h>
#include <plat/system.h>
#include <mach/irqs.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/delay.h>

#define SUNXI_LOSC_CTRL_REG               	(0x100)

#define SUNXI_RTC_DATE_REG                	(0x104)
#define SUNXI_RTC_TIME_REG                	(0x108)

#define SUNXI_RTC_ALARM_DD_HH_MM_SS_REG   	(0x10C)

#define SUNXI_ALARM_EN_REG                	(0x114)
#define SUNXI_ALARM_INT_CTRL_REG          	(0x118)
#define SUNXI_ALARM_INT_STATUS_REG        	(0x11C)

/*interrupt control
*rtc week interrupt control
*/
#define RTC_ENABLE_WK_IRQ            		0x00000002

/*rtc count interrupt control*/
#define RTC_ALARM_COUNT_INT_EN 				0x00000100

#define RTC_ENABLE_CNT_IRQ        			0x00000001

/*Crystal Control*/
#define REG_LOSCCTRL_MAGIC		    		0x16aa0000
#define REG_CLK32K_AUTO_SWT_EN  			(0x00004000)
#define RTC_SOURCE_EXTERNAL         		0x00000001
#define RTC_HHMMSS_ACCESS           		0x00000100
#define RTC_YYMMDD_ACCESS           		0x00000080
#define EXT_LOSC_GSM                    	(0x00000008)

/*Date Value*/
#define IS_LEAP_YEAR(year) \
	(((year) % 400) == 0 || (((year) % 100) != 0 && ((year) % 4) == 0))

#define DATA_YEAR_MIN			(sunxi_is_sun7i() ? 1970 : 2010)
#define DATA_YEAR_MAX			(sunxi_is_sun7i() ? 2100 : 2073)
#define DATA_YEAR_OFFSET		(sunxi_is_sun7i() ? 0 : 110)
#define DATA_YEAR_MASK			(sunxi_is_sun7i() ? 0xff : 0x3f)
#define LEAP_BIT			(sunxi_is_sun7i() ? 24 : 22)

#define DATE_GET_DAY_VALUE(x)       		((x) &0x0000001f)
#define DATE_GET_MON_VALUE(x)       		(((x)&0x00000f00) >> 8 )
#define DATE_GET_YEAR_VALUE(x)      		(((x) >> 16) & DATA_YEAR_MASK)

#define DATE_SET_DAY_VALUE(x)       		DATE_GET_DAY_VALUE(x)
#define DATE_SET_MON_VALUE(x)       		(((x)&0x0000000f) << 8 )
#define DATE_SET_YEAR_VALUE(x)      		(((x) & DATA_YEAR_MASK) << 16)
#define LEAP_SET_VALUE(x)           		(((x)&0x00000001) << LEAP_BIT)

/*Time Value*/
#define TIME_GET_SEC_VALUE(x)       		((x) &0x0000003f)
#define TIME_GET_MIN_VALUE(x)       		(((x)&0x00003f00) >> 8 )
#define TIME_GET_HOUR_VALUE(x)      		(((x)&0x001f0000) >> 16)

#define TIME_SET_SEC_VALUE(x)       		TIME_GET_SEC_VALUE(x)
#define TIME_SET_MIN_VALUE(x)       		(((x)&0x0000003f) << 8 )
#define TIME_SET_HOUR_VALUE(x)      		(((x)&0x0000001f) << 16)

/*ALARM Value*/
#define ALARM_GET_SEC_VALUE(x)      		((x) &0x0000003f)
#define ALARM_GET_MIN_VALUE(x)      		(((x)&0x00003f00) >> 8 )
#define ALARM_GET_HOUR_VALUE(x)     		(((x)&0x001f0000) >> 16)

#define ALARM_SET_SEC_VALUE(x)      		((x) &0x0000003f)
#define ALARM_SET_MIN_VALUE(x)      		(((x)&0x0000003f) << 8 )
#define ALARM_SET_HOUR_VALUE(x)     		(((x)&0x0000001f) << 16)
#define ALARM_SET_DAY_VALUE(x)      		(((x)&0x000000ff) << 24)

#define PWM_CTRL_REG_BASE         			(0xf1c20c00+0x200)

//#define RTC_ALARM_DEBUG
/*
 * notice: IN 23 A version, operation(eg. write date, time reg)
 * that will affect losc reg, will also affect pwm reg at the same time
 * it is a ic bug needed to be fixed,
 * right now, before write date, time reg, we need to backup pwm reg
 * after writing, we should restore pwm reg.
 */
//#define BACKUP_PWM

#define IRIS_RTC_M41T11_ADDR		0x68
#define IRIS_RTC_M41T11_I2CBUS		1

#define IRIS_RTC_M41T11_REG_SECS		0x00	/* 00-59 */
#define IRIS_RTC_M41T11_BIT_CH			0x80
#define IRIS_RTC_M41T11_REG_MIN			0x01	/* 00-59 */
#define IRIS_RTC_M41T11_REG_HOUR		0x02	/* 00-23, or 1-12{am,pm} */
#define IRIS_RTC_M41T11_BIT_12HR		0x40	/* in REG_HOUR */
#define IRIS_RTC_M41T11_BIT_PM			0x20	/* in REG_HOUR */
#define IRIS_RTC_M41T11_REG_WDAY		0x03	/* 01-07 */
#define IRIS_RTC_M41T11_REG_MDAY		0x04	/* 01-31 */
#define IRIS_RTC_M41T11_REG_MONTH		0x05	/* 01-12 */
#define IRIS_RTC_M41T11_REG_YEAR		0x06	/* 00-99 */

#define IRIS_RTC_M41T11_REG_CONTROL		0x07
#define IRIS_RTC_M41T11_BIT_OUT			0x80
#define IRIS_RTC_M41T11_BIT_SQWE		0x10
#define IRIS_RTC_M41T11_BIT_RS1			0x02
#define IRIS_RTC_M41T11_BIT_RS0			0x01

#define BLOCK_DATA_MAX_TRIES 10

#define HAS_NVRAM	0	/* bit 0 == sysfs file active */


struct iris_m41t11 {
	u8			offset;
	u8			regs[11];
	u16			nvram_offset;
	struct bin_attribute	*nvram;
	u32			type;
	unsigned long		flags;
	struct i2c_client	*client;
	struct rtc_device	*rtc;
	struct work_struct	work;
	s32 (*rdata)(const struct i2c_client *client, u8 command, u8 length, u8 *values);
	s32 (*wdata)(const struct i2c_client *client, u8 command, u8 length, const u8 *values);
};

struct chip_desc {
	unsigned		alarm:1;
	u16			nvram_offset;
	u16			nvram_size;
};

static const struct chip_desc chips[] = {
	{
		.nvram_offset	= 8,
		.nvram_size	= 56,
	},
	{ }
};

static const struct i2c_device_id iris_m41t11_id[] = {
	{ "iris_m41t11", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, iris_m41t11_id);

/* record rtc device handle for platform to restore system time */
struct rtc_device   *sw_rtc_dev = NULL;

/*?明 sunxi最大?化?63年的??
???支持（2010～2073）年的??*/
int main_rtc_selection = 1;
static void __iomem *sunxi_rtc_base;

static int sunxi_rtc_alarmno = NO_IRQ;
static int losc_err_flag   = 0;

int i2c_bus = IRIS_RTC_M41T11_I2CBUS;

static s32 iris_m41t11_rdata_once(const struct i2c_client *client, u8 command, u8 length, u8 *values)
{
	s32 i, data;

	for (i = 0; i < length; i++) {
		data = i2c_smbus_read_byte_data(client, command + i);
		if (data < 0)
			return data;
		values[i] = data;
	}
	return i;
}

static s32 iris_m41t11_rdata(const struct i2c_client *client, u8 command, u8 length, u8 *values)
{
	u8 oldvalues[I2C_SMBUS_BLOCK_MAX];
	s32 ret;
	int tries = 0;

	ret = iris_m41t11_rdata_once(client, command, length, values);
	if (ret < 0)
		return ret;
	do {
		if (++tries > BLOCK_DATA_MAX_TRIES) {
			dev_err(&client->dev, "iris_m41t11_rdata failed\n");
			return -EIO;
		}
		memcpy(oldvalues, values, length);
		ret = iris_m41t11_rdata_once(client, command, length, values);
		if (ret < 0)
			return ret;
	} while (memcmp(oldvalues, values, length));
	return length;
}

static s32 iris_m41t11_wdata(const struct i2c_client *client, u8 command,
				   u8 length, const u8 *values)
{
	u8 currvalues[I2C_SMBUS_BLOCK_MAX];
	int tries = 0;

	do {
		s32 i, ret;

		if (++tries > BLOCK_DATA_MAX_TRIES) {
			dev_err(&client->dev, "iris_m41t11_wdata failed\n");
			return -EIO;
		}
		for (i = 0; i < length; i++) {
			ret = i2c_smbus_write_byte_data(client, command + i, values[i]);
			if (ret < 0)
				return ret;
		}
		ret = iris_m41t11_rdata_once(client, command, length, currvalues);
		if (ret < 0)
			return ret;
	} while (memcmp(currvalues, values, length));
	return length;
}

static int iris_m41t11_get_time(struct device *dev, struct rtc_time *t)
{
	struct iris_m41t11	*iris_m41t11 = dev_get_drvdata(dev);
	int		tmp;

	tmp = iris_m41t11->rdata(iris_m41t11->client, iris_m41t11->offset, 7, iris_m41t11->regs);
	if (tmp != 7) {
		dev_err(dev, "%s error %d\n", "read", tmp);
		return -EIO;
	}

	dev_dbg(dev, "%s: %02x %02x %02x %02x %02x %02x %02x\n",
			"read",
			iris_m41t11->regs[0], iris_m41t11->regs[1],
			iris_m41t11->regs[2], iris_m41t11->regs[3],
			iris_m41t11->regs[4], iris_m41t11->regs[5],
			iris_m41t11->regs[6]);

	t->tm_sec = bcd2bin(iris_m41t11->regs[IRIS_RTC_M41T11_REG_SECS] & 0x7f);
	t->tm_min = bcd2bin(iris_m41t11->regs[IRIS_RTC_M41T11_REG_MIN] & 0x7f);
	tmp = iris_m41t11->regs[IRIS_RTC_M41T11_REG_HOUR] & 0x3f;
	t->tm_hour = bcd2bin(tmp);
	t->tm_wday = bcd2bin(iris_m41t11->regs[IRIS_RTC_M41T11_REG_WDAY] & 0x07) - 1;
	t->tm_mday = bcd2bin(iris_m41t11->regs[IRIS_RTC_M41T11_REG_MDAY] & 0x3f);
	tmp = iris_m41t11->regs[IRIS_RTC_M41T11_REG_MONTH] & 0x1f;
	t->tm_mon = bcd2bin(tmp) - 1;

	t->tm_year = bcd2bin(iris_m41t11->regs[IRIS_RTC_M41T11_REG_YEAR]) + 100;

	dev_dbg(dev, "%s secs=%d, mins=%d, "
		"hours=%d, mday=%d, mon=%d, year=%d, wday=%d\n",
		"read", t->tm_sec, t->tm_min,
		t->tm_hour, t->tm_mday,
		t->tm_mon, t->tm_year, t->tm_wday);

	return rtc_valid_tm(t);
}

static int iris_m41t11_set_time(struct device *dev, struct rtc_time *t)
{
	struct iris_m41t11	*iris_m41t11 = dev_get_drvdata(dev);
	int		result;
	int		tmp;
	u8		*buf = iris_m41t11->regs;

	dev_dbg(dev, "%s secs=%d, mins=%d, "
		"hours=%d, mday=%d, mon=%d, year=%d, wday=%d\n",
		"write", t->tm_sec, t->tm_min,
		t->tm_hour, t->tm_mday,
		t->tm_mon, t->tm_year, t->tm_wday);

	buf[IRIS_RTC_M41T11_REG_SECS] = bin2bcd(t->tm_sec);
	buf[IRIS_RTC_M41T11_REG_MIN] = bin2bcd(t->tm_min);
	buf[IRIS_RTC_M41T11_REG_HOUR] = bin2bcd(t->tm_hour);
	buf[IRIS_RTC_M41T11_REG_WDAY] = bin2bcd(t->tm_wday + 1);
	buf[IRIS_RTC_M41T11_REG_MDAY] = bin2bcd(t->tm_mday);
	buf[IRIS_RTC_M41T11_REG_MONTH] = bin2bcd(t->tm_mon + 1);

	tmp = t->tm_year - 100;
	buf[IRIS_RTC_M41T11_REG_YEAR] = bin2bcd(tmp);

	dev_dbg(dev, "%s: %02x %02x %02x %02x %02x %02x %02x\n",
		"write", buf[0], buf[1], buf[2], buf[3],
		buf[4], buf[5], buf[6]);

	result = iris_m41t11->wdata(iris_m41t11->client, iris_m41t11->offset, 7, buf);
	if (result < 0) {
		dev_err(dev, "%s error %d\n", "write", result);
		return result;
	}
	return 0;
}


static const struct rtc_class_ops m41t11_rtc_ops = {
	.read_time	= iris_m41t11_get_time,
	.set_time	= iris_m41t11_set_time,
};


static ssize_t iris_m41t11_nvram_read(struct file *filp, struct kobject *kobj,
		struct bin_attribute *attr,
		char *buf, loff_t off, size_t count)
{
	struct i2c_client	*client;
	struct iris_m41t11		*iris_m41t11;
	int			result;

	client = kobj_to_i2c_client(kobj);
	iris_m41t11 = i2c_get_clientdata(client);

	if (unlikely(off >= iris_m41t11->nvram->size))
		return 0;
	if ((off + count) > iris_m41t11->nvram->size)
		count = iris_m41t11->nvram->size - off;
	if (unlikely(!count))
		return count;

	result = iris_m41t11->rdata(client, iris_m41t11->nvram_offset + off,
								count, buf);
	if (result < 0)
		dev_err(&client->dev, "%s error %d\n", "nvram read", result);
	return result;
}

static ssize_t
iris_m41t11_nvram_write(struct file *filp, struct kobject *kobj,
		struct bin_attribute *attr,
		char *buf, loff_t off, size_t count)
{
	struct i2c_client	*client;
	struct iris_m41t11		*iris_m41t11;
	int			result;

	client = kobj_to_i2c_client(kobj);
	iris_m41t11 = i2c_get_clientdata(client);

	if (unlikely(off >= iris_m41t11->nvram->size))
		return -EFBIG;
	if ((off + count) > iris_m41t11->nvram->size)
		count = iris_m41t11->nvram->size - off;
	if (unlikely(!count))
		return count;

	result = iris_m41t11->wdata(client, iris_m41t11->nvram_offset + off, count, buf);
	if (result < 0) {
		dev_err(&client->dev, "%s error %d\n", "nvram write", result);
		return result;
	}
	return count;
}
struct iris_m41t11		*gIris_m41t11;

static int __devinit iris_m41t11_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct iris_m41t11		*iris_m41t11;
	int			err = -ENODEV;
	int			tmp;
	const struct chip_desc	*chip = &chips[id->driver_data];
	struct i2c_adapter	*adapter = to_i2c_adapter(client->dev.parent);
	unsigned char		*buf;

	if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA)
	    && !i2c_check_functionality(adapter, I2C_FUNC_SMBUS_I2C_BLOCK))
		return -EIO;

	gIris_m41t11 = iris_m41t11 = kzalloc(sizeof(struct iris_m41t11), GFP_KERNEL);

	if (!iris_m41t11)
		return -ENOMEM;

	i2c_set_clientdata(client, iris_m41t11);

	iris_m41t11->client	= client;
	iris_m41t11->type	= id->driver_data;
	iris_m41t11->offset	= 0;

	buf = iris_m41t11->regs;
	if (i2c_check_functionality(adapter, I2C_FUNC_SMBUS_I2C_BLOCK)) {
		iris_m41t11->rdata = i2c_smbus_read_i2c_block_data;
		iris_m41t11->wdata = i2c_smbus_write_i2c_block_data;
	} else {
		iris_m41t11->rdata = iris_m41t11_rdata;
		iris_m41t11->wdata = iris_m41t11_wdata;
	}

read_rtc:

	tmp = iris_m41t11->rdata(iris_m41t11->client, iris_m41t11->offset, 8, buf);
	if (tmp != 8) {
		pr_debug("read error %d\n", tmp);
		err = -EIO;
		goto exit_free;
	}

	tmp = iris_m41t11->regs[IRIS_RTC_M41T11_REG_SECS];

	if (tmp & IRIS_RTC_M41T11_BIT_CH) {
		i2c_smbus_write_byte_data(client, IRIS_RTC_M41T11_REG_SECS, 0);
		dev_warn(&client->dev, "SET TIME!\n");
		goto read_rtc;
	}

	tmp = iris_m41t11->regs[IRIS_RTC_M41T11_REG_HOUR];


	if ((tmp & IRIS_RTC_M41T11_BIT_12HR)) {
		tmp = bcd2bin(tmp & 0x1f);
		if (tmp == 12)
			tmp = 0;
		if (iris_m41t11->regs[IRIS_RTC_M41T11_REG_HOUR] & IRIS_RTC_M41T11_BIT_PM)
			tmp += 12;
		i2c_smbus_write_byte_data(client, iris_m41t11->offset + IRIS_RTC_M41T11_REG_HOUR, bin2bcd(tmp));
	}

	iris_m41t11->rtc = rtc_device_register(client->name, &client->dev, &m41t11_rtc_ops, THIS_MODULE);

	if (IS_ERR(iris_m41t11->rtc)) {
		err = PTR_ERR(iris_m41t11->rtc);
		dev_err(&client->dev, "unable to register the class device\n");
		goto exit_free;
	}

	if (chip->nvram_size) {
		iris_m41t11->nvram = kzalloc(sizeof(struct bin_attribute),
							GFP_KERNEL);
		if (!iris_m41t11->nvram) {
			err = -ENOMEM;
			goto exit_nvram;
		}
		iris_m41t11->nvram->attr.name = "nvram";
		iris_m41t11->nvram->attr.mode = S_IRUGO | S_IWUSR;
		sysfs_bin_attr_init(iris_m41t11->nvram);
		iris_m41t11->nvram->read = iris_m41t11_nvram_read,
		iris_m41t11->nvram->write = iris_m41t11_nvram_write,
		iris_m41t11->nvram->size = chip->nvram_size;
		iris_m41t11->nvram_offset = chip->nvram_offset;
		err = sysfs_create_bin_file(&client->dev.kobj, iris_m41t11->nvram);
		if (err) {
			kfree(iris_m41t11->nvram);
			goto exit_nvram;
		}
		set_bit(HAS_NVRAM, &iris_m41t11->flags);
		dev_info(&client->dev, "%zu bytes nvram\n", iris_m41t11->nvram->size);
	}

	return 0;

exit_nvram:

exit_free:
	kfree(iris_m41t11);
	return err;
}

static int __devexit iris_m41t11_remove(struct i2c_client *client)
{
	struct iris_m41t11 *iris_m41t11 = i2c_get_clientdata(client);

	if (test_and_clear_bit(HAS_NVRAM, &iris_m41t11->flags)) {
		sysfs_remove_bin_file(&client->dev.kobj, iris_m41t11->nvram);
		kfree(iris_m41t11->nvram);
	}

	rtc_device_unregister(iris_m41t11->rtc);
	kfree(iris_m41t11);
	return 0;
}

static struct i2c_driver iris_m41t11_driver = {
	.driver = {
		.name	= "rtc-iris_m41t11",
		.owner	= THIS_MODULE,
	},
	.probe		= iris_m41t11_probe,
	.remove		= __devexit_p(iris_m41t11_remove),
	.id_table	= iris_m41t11_id,
};

/* IRQ Handlers, irq no. is shared with timer2 */
static irqreturn_t sunxi_rtc_alarmirq(int irq, void *id)
{
	struct rtc_device *rdev = id;
	u32 val;

    /*judge the int is whether ours*/
    val = readl(sunxi_rtc_base + SUNXI_ALARM_INT_STATUS_REG)&(RTC_ENABLE_WK_IRQ | RTC_ENABLE_CNT_IRQ);
    if (val) {
		/*Clear pending count alarm*/
		val = readl(sunxi_rtc_base + SUNXI_ALARM_INT_STATUS_REG);//0x11c
		val |= (RTC_ENABLE_CNT_IRQ);	//0x00000001
		writel(val, sunxi_rtc_base + SUNXI_ALARM_INT_STATUS_REG);

		rtc_update_irq(rdev, 1, RTC_AF | RTC_IRQF);
		return IRQ_HANDLED;
    } else {
        return IRQ_NONE;
    }
}

/* Update control registers,asynchronous interrupt enable*/
static void sunxi_rtc_setaie(int to)
{
	u32 alarm_irq_val;

#ifdef RTC_ALARM_DEBUG
	printk("%s: aie=%d\n", __func__, to);
#endif

	alarm_irq_val = readl(sunxi_rtc_base + SUNXI_ALARM_EN_REG);
	switch(to){
		case 1:
		alarm_irq_val |= RTC_ALARM_COUNT_INT_EN;		//0x00000100
	    writel(alarm_irq_val, sunxi_rtc_base + SUNXI_ALARM_EN_REG);//0x114
		break;
		case 0:
		default:
		alarm_irq_val = 0x00000000;
	    writel(alarm_irq_val, sunxi_rtc_base + SUNXI_ALARM_EN_REG);//0x114
		break;
	}
}

/* Time read/write */
static int sunxi_rtc_gettime(struct device *dev, struct rtc_time *rtc_tm)
{
	unsigned int have_retried = 0;
	void __iomem *base = sunxi_rtc_base;
	unsigned int date_tmp = 0;
	unsigned int time_tmp = 0;

retry_get_time:
	if (losc_err_flag) {
		rtc_tm->tm_sec  = 0;
		rtc_tm->tm_min  = 0;
		rtc_tm->tm_hour = 0;

		rtc_tm->tm_mday = 1;
		rtc_tm->tm_mon  = 0;
		rtc_tm->tm_year = 110;
		/* 2010-Jan-1 is better then 1970-Jan-1, esp. because of:
		 * http://tree.celinuxforum.org/CelfPubWiki/Ext3OrphanedInodeProblem */
		return 0; 
	}

	/* first to get the date, then time, because the sec turn to 0 will
	 * effect the date */
	date_tmp = readl(base + SUNXI_RTC_DATE_REG);
	time_tmp = readl(base + SUNXI_RTC_TIME_REG);

	rtc_tm->tm_sec  = TIME_GET_SEC_VALUE(time_tmp);
	rtc_tm->tm_min  = TIME_GET_MIN_VALUE(time_tmp);
	rtc_tm->tm_hour = TIME_GET_HOUR_VALUE(time_tmp);

	rtc_tm->tm_mday = DATE_GET_DAY_VALUE(date_tmp);
	rtc_tm->tm_mon  = DATE_GET_MON_VALUE(date_tmp);
	rtc_tm->tm_year = DATE_GET_YEAR_VALUE(date_tmp);

	/* the only way to work out wether the system was mid-update
	 * when we read it is to check the second counter, and if it
	 * is zero, then we re-try the entire read
	 */
	if (rtc_tm->tm_sec == 0 && !have_retried) {
		have_retried = 1;
		goto retry_get_time;
	}

	rtc_tm->tm_year += DATA_YEAR_OFFSET;
	rtc_tm->tm_mon  -= 1;

	if (rtc_tm->tm_year < 70) {
		dev_err(dev, "Warning: RTC time is wrong!\n");
		losc_err_flag = 1;
		goto retry_get_time;
	}

	return 0;
}

static int sunxi_rtc_settime(struct device *dev, struct rtc_time *tm)
{
	void __iomem *base = sunxi_rtc_base;
	unsigned int date_tmp = 0;
	unsigned int time_tmp = 0;
	unsigned int crystal_data = 0;
	unsigned int timeout = 0;
	int actual_year = 0;
	int line = 0;

#ifdef BACKUP_PWM
	unsigned int pwm_ctrl_reg_backup = 0;
	unsigned int pwm_ch0_period_backup = 0;
#endif

	/* int tm_year; years from 1900
	 * int tm_mon; months since january 0-11
	 * the input para tm->tm_year is the offset related 1900;
	 */
	actual_year = tm->tm_year + 1900;
	if (actual_year < DATA_YEAR_MIN || actual_year > DATA_YEAR_MAX) {
		dev_err(dev, "rtc only supports years between %d - %d\n",
			DATA_YEAR_MIN, DATA_YEAR_MAX);
		return -EINVAL;
	}

	crystal_data = readl(base + SUNXI_LOSC_CTRL_REG);

	/*Any bit of [9:7] is set, The time and date
	* register can`t be written, we re-try the entried read
	*/
	{
	    /*check at most 3 times.*/
	    int times = 3;
	    while((crystal_data & 0x380) && (times--)){
	    	dev_info(dev, "cannot change rtc now!\n");
	    	msleep(500);
	    	crystal_data = readl(base + SUNXI_LOSC_CTRL_REG);
	    }
	}

	tm->tm_year -= DATA_YEAR_OFFSET;
	tm->tm_mon  += 1;

	/* prevent the application seting an invalid time */
	switch (tm->tm_mon) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		if (tm->tm_mday > 31)
			line = __LINE__;
		break;
	case 4:
	case 6:
	case 9:
	case 11:
		if (tm->tm_mday > 30)
			line = __LINE__;
		break;
	case 2:
		if (IS_LEAP_YEAR(actual_year)) {
			if (tm->tm_mday > 29)
				line = __LINE__;
		} else {
			if (tm->tm_mday > 28)
				line = __LINE__;
		}
		break;
	default:
		line = __LINE__;
		break;
	}
	if (tm->tm_hour > 24 || tm->tm_min > 59 || tm->tm_sec > 59)
		line = __LINE__;
	if (0 != line) {
		dev_err(dev, "err: date %d-%d-%d %d:%d:%d, so reset to 2010-1-1 00:00:00\n",
			tm->tm_year + 1900, tm->tm_mon, tm->tm_mday,
			tm->tm_hour, tm->tm_min, tm->tm_sec);
		tm->tm_sec  = 0;
		tm->tm_min  = 0;
		tm->tm_hour = 0;
		tm->tm_mday = 0;
		tm->tm_mon  = 0;
		tm->tm_year = 110 - DATA_YEAR_OFFSET;
	}

	dev_info(dev, "actually set time to %d-%d-%d %d:%d:%d\n",
		tm->tm_year + 1900 + DATA_YEAR_OFFSET,
		tm->tm_mon, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);

	/* set hour, minute, second */
	date_tmp = (DATE_SET_DAY_VALUE(tm->tm_mday)|DATE_SET_MON_VALUE(tm->tm_mon)
                    |DATE_SET_YEAR_VALUE(tm->tm_year));

	time_tmp = (TIME_SET_SEC_VALUE(tm->tm_sec)|TIME_SET_MIN_VALUE(tm->tm_min)
                    |TIME_SET_HOUR_VALUE(tm->tm_hour));

#ifdef BACKUP_PWM
    pwm_ctrl_reg_backup = readl(PWM_CTRL_REG_BASE + 0);
    pwm_ch0_period_backup = readl(PWM_CTRL_REG_BASE + 4);
	printk("[rtc-pwm] 1 pwm_ctrl_reg_backup = %x pwm_ch0_period_backup = %x", pwm_ctrl_reg_backup, pwm_ch0_period_backup);
#endif

	writel(time_tmp,  base + SUNXI_RTC_TIME_REG);
	timeout = 0xffff;
	while((readl(base + SUNXI_LOSC_CTRL_REG)&(RTC_HHMMSS_ACCESS))&&(--timeout))
	if (timeout == 0) {
        dev_err(dev, "fail to set rtc time.\n");

#ifdef BACKUP_PWM
	    writel(pwm_ctrl_reg_backup, PWM_CTRL_REG_BASE + 0);
	    writel(pwm_ch0_period_backup, PWM_CTRL_REG_BASE + 4);

		pwm_ctrl_reg_backup = readl(PWM_CTRL_REG_BASE + 0);
    	pwm_ch0_period_backup = readl(PWM_CTRL_REG_BASE + 4);
		printk("[rtc-pwm] 2 pwm_ctrl_reg_backup = %x pwm_ch0_period_backup = %x", pwm_ctrl_reg_backup, pwm_ch0_period_backup);
#endif

        return -1;
    }

	if (IS_LEAP_YEAR(actual_year)) {
		/*Set Leap Year bit*/
		date_tmp |= LEAP_SET_VALUE(1);
	}

	writel(date_tmp, base + SUNXI_RTC_DATE_REG);
	timeout = 0xffff;
	while((readl(base + SUNXI_LOSC_CTRL_REG)&(RTC_YYMMDD_ACCESS))&&(--timeout))
	if (timeout == 0) {
        dev_err(dev, "fail to set rtc date.\n");

#ifdef BACKUP_PWM
        writel(pwm_ctrl_reg_backup, PWM_CTRL_REG_BASE + 0);
        writel(pwm_ch0_period_backup, PWM_CTRL_REG_BASE + 4);

		pwm_ctrl_reg_backup = readl(PWM_CTRL_REG_BASE + 0);
	    pwm_ch0_period_backup = readl(PWM_CTRL_REG_BASE + 4);
	    printk("[rtc-pwm] 5 pwm_ctrl_reg_backup = %x pwm_ch0_period_backup = %x", pwm_ctrl_reg_backup, pwm_ch0_period_backup);
#endif

        return -1;
    }

#ifdef BACKUP_PWM
       writel(pwm_ctrl_reg_backup, PWM_CTRL_REG_BASE + 0);
       writel(pwm_ch0_period_backup, PWM_CTRL_REG_BASE + 4);

 		pwm_ctrl_reg_backup = readl(PWM_CTRL_REG_BASE + 0);
	    pwm_ch0_period_backup = readl(PWM_CTRL_REG_BASE + 4);
	    printk("[rtc-pwm] 6 pwm_ctrl_reg_backup = %x pwm_ch0_period_backup = %x", pwm_ctrl_reg_backup, pwm_ch0_period_backup);
#endif

    /*wait about 70us to make sure the the time is really written into target.*/
    udelay(70);

	losc_err_flag = 0;
	return 0;
}

static int sunxi_rtc_getalarm(struct device *dev, struct rtc_wkalrm *alrm)
{
	struct rtc_time *alm_tm = &alrm->time;
	void __iomem *base = sunxi_rtc_base;
	unsigned int alarm_en;
	unsigned int alarm_tmp = 0;
	unsigned int date_tmp = 0;

    alarm_tmp = readl(base + SUNXI_RTC_ALARM_DD_HH_MM_SS_REG);
	date_tmp = readl(base + SUNXI_RTC_DATE_REG);

    alm_tm->tm_sec  = ALARM_GET_SEC_VALUE(alarm_tmp);
    alm_tm->tm_min  = ALARM_GET_MIN_VALUE(alarm_tmp);
    alm_tm->tm_hour = ALARM_GET_HOUR_VALUE(alarm_tmp);

	alm_tm->tm_mday = DATE_GET_DAY_VALUE(date_tmp);
	alm_tm->tm_mon  = DATE_GET_MON_VALUE(date_tmp);
	alm_tm->tm_year = DATE_GET_YEAR_VALUE(date_tmp);

    alm_tm->tm_year += DATA_YEAR_OFFSET;
    alm_tm->tm_mon  -= 1;

    alarm_en = readl(base + SUNXI_ALARM_INT_CTRL_REG);
    if(alarm_en&&RTC_ALARM_COUNT_INT_EN)
    	alrm->enabled = 1;

	return 0;
}

static int sunxi_rtc_setalarm(struct device *dev, struct rtc_wkalrm *alrm)
{
    struct rtc_time *tm = &alrm->time;
    void __iomem *base = sunxi_rtc_base;
    unsigned int alarm_tmp = 0;
    unsigned int alarm_en;
    int ret = 0;
    struct rtc_time tm_now;
    unsigned long time_now = 0;
    unsigned long time_set = 0;
    unsigned long time_gap = 0;
    unsigned long time_gap_day = 0;
    unsigned long time_gap_hour = 0;
    unsigned long time_gap_minute = 0;
    unsigned long time_gap_second = 0;

#ifdef RTC_ALARM_DEBUG
    printk("*****************************\n\n");
    printk("line:%d,%s the alarm time: year:%d, month:%d, day:%d. hour:%d.minute:%d.second:%d\n",\
    __LINE__, __func__, tm->tm_year, tm->tm_mon,\
    	 tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
   	printk("*****************************\n\n");
#endif

    ret = sunxi_rtc_gettime(dev, &tm_now);

#ifdef RTC_ALARM_DEBUG
    printk("line:%d,%s the current time: year:%d, month:%d, day:%d. hour:%d.minute:%d.second:%d\n",\
    __LINE__, __func__, tm_now.tm_year, tm_now.tm_mon,\
    	 tm_now.tm_mday, tm_now.tm_hour, tm_now.tm_min, tm_now.tm_sec);
   	printk("*****************************\n\n");
#endif

    ret = rtc_tm_to_time(tm, &time_set);
    ret = rtc_tm_to_time(&tm_now, &time_now);
    if(time_set <= time_now){
    	dev_err(dev, "The time or date can`t set, The day has pass!!!\n");
    	return -EINVAL;
    }
    time_gap = time_set - time_now;
    time_gap_day = time_gap/(3600*24);//day
    time_gap_hour = (time_gap - time_gap_day*24*60*60)/3600;//hour
    time_gap_minute = (time_gap - time_gap_day*24*60*60 - time_gap_hour*60*60)/60;//minute
    time_gap_second = time_gap - time_gap_day*24*60*60 - time_gap_hour*60*60-time_gap_minute*60;//second
    if(time_gap_day > 255) {
    	dev_err(dev, "The time or date can`t set, The day range of 0 to 255\n");
    	return -EINVAL;
    }

#ifdef RTC_ALARM_DEBUG
   	printk("line:%d,%s year:%d, month:%d, day:%ld. hour:%ld.minute:%ld.second:%ld\n",\
    __LINE__, __func__, tm->tm_year, tm->tm_mon,\
    	 time_gap_day, time_gap_hour, time_gap_minute, time_gap_second);
    printk("*****************************\n\n");
#endif

	/*clear the alarm counter enable bit*/
    sunxi_rtc_setaie(0);

    /*clear the alarm count value!!!*/
    writel(0x00000000, base + SUNXI_RTC_ALARM_DD_HH_MM_SS_REG);
    __udelay(100);

    /*rewrite the alarm count value!!!*/
    alarm_tmp = ALARM_SET_SEC_VALUE(time_gap_second) | ALARM_SET_MIN_VALUE(time_gap_minute)
    	| ALARM_SET_HOUR_VALUE(time_gap_hour) | ALARM_SET_DAY_VALUE(time_gap_day);
    writel(alarm_tmp, base + SUNXI_RTC_ALARM_DD_HH_MM_SS_REG);//0x10c

    /*clear the count enable alarm irq bit*/
    writel(0x00000000, base + SUNXI_ALARM_INT_CTRL_REG);
	alarm_en = readl(base + SUNXI_ALARM_INT_CTRL_REG);//0x118

	/*enable the counter alarm irq*/
	alarm_en = readl(base + SUNXI_ALARM_INT_CTRL_REG);//0x118
	alarm_en |= RTC_ENABLE_CNT_IRQ;
    writel(alarm_en, base + SUNXI_ALARM_INT_CTRL_REG);//enable the counter irq!!!

	if(alrm->enabled != 1){
		printk("warning:the rtc counter interrupt isnot enable!!!,%s,%d\n", __func__, __LINE__);
	}

	/*decided whether we should start the counter to down count*/
	sunxi_rtc_setaie(alrm->enabled);

#ifdef RTC_ALARM_DEBUG
	printk("------------------------------------------\n\n");
	printk("%d,10c reg val:%x\n", __LINE__, *(volatile int *)(0xf1c20c00+0x10c));
	printk("%d,114 reg val:%x\n", __LINE__, *(volatile int *)(0xf1c20c00+0x114));
	printk("%d,118 reg val:%x\n", __LINE__, *(volatile int *)(0xf1c20c00+0x118));
	printk("%d,11c reg val:%x\n", __LINE__, *(volatile int *)(0xf1c20c00+0x11c));
	printk("------------------------------------------\n\n");
#endif

	return 0;
}

static int sunxi_rtc_open(struct device *dev)
{
	return 0;
}

static void sunxi_rtc_release(struct device *dev)
{

}

static const struct rtc_class_ops sunxi_rtcops = {
	.open				= sunxi_rtc_open,
	.release			= sunxi_rtc_release,
	.read_time			= sunxi_rtc_gettime,
	.set_time			= sunxi_rtc_settime,
	.read_alarm			= sunxi_rtc_getalarm,
	.set_alarm			= sunxi_rtc_setalarm,
};

static int __devexit sunxi_rtc_remove(struct platform_device *pdev)
{
	struct rtc_device *rtc = platform_get_drvdata(pdev);
    free_irq(sunxi_rtc_alarmno, rtc);
    rtc_device_unregister(rtc);
	platform_set_drvdata(pdev, NULL);

	sunxi_rtc_setaie(0);

	return 0;
}

static int __devinit sunxi_rtc_probe(struct platform_device *pdev)
{
	struct rtc_device *rtc;
	int ret;
	unsigned int tmp_data;

#ifdef BACKUP_PWM
	unsigned int pwm_ctrl_reg_backup = 0;
	unsigned int pwm_ch0_period_backup = 0;
#endif

	sunxi_rtc_base = (void __iomem *)(SW_VA_TIMERC_IO_BASE);
	sunxi_rtc_alarmno = SW_INT_IRQNO_ALARM;

	/* select RTC clock source
	*  on fpga board, internal 32k clk src is the default, and can not be changed
	*
	*  RTC CLOCK SOURCE internal 32K HZ
	*/
#ifdef BACKUP_PWM
	pwm_ctrl_reg_backup = readl(PWM_CTRL_REG_BASE + 0);
	pwm_ch0_period_backup = readl(PWM_CTRL_REG_BASE + 4);
	printk("[rtc-pwm] 1 pwm_ctrl_reg_backup = %x pwm_ch0_period_backup = %x", pwm_ctrl_reg_backup, pwm_ch0_period_backup);
#endif

	/*	upate by kevin, 2011-9-7 18:23
	*	step1: set keyfiled,??外部晶振
	*/
	tmp_data = readl(sunxi_rtc_base + SUNXI_LOSC_CTRL_REG);
	tmp_data &= (~REG_CLK32K_AUTO_SWT_EN);            		//disable auto switch,bit-14
	tmp_data |= (RTC_SOURCE_EXTERNAL | REG_LOSCCTRL_MAGIC); //external     32768hz osc
	tmp_data |= (EXT_LOSC_GSM);                             //external 32768hz osc gsm
	writel(tmp_data, sunxi_rtc_base + SUNXI_LOSC_CTRL_REG);
	__udelay(100);

	/*step2: check set result，??是否?置成功*/
	tmp_data = readl(sunxi_rtc_base + SUNXI_LOSC_CTRL_REG);
	if(!(tmp_data & RTC_SOURCE_EXTERNAL)){
		dev_err(&pdev->dev, "Error: Set LOSC to external failed.\n");
		dev_err(&pdev->dev, "Warning: RTC time will be wrong!\n");
		losc_err_flag = 1;
	}

	sunxi_rtc_setaie(0);

#ifdef BACKUP_PWM
	writel(pwm_ctrl_reg_backup, PWM_CTRL_REG_BASE + 0);
	writel(pwm_ch0_period_backup, PWM_CTRL_REG_BASE + 4);
	pwm_ctrl_reg_backup = readl(PWM_CTRL_REG_BASE + 0);
	pwm_ch0_period_backup = readl(PWM_CTRL_REG_BASE + 4);
	printk("[rtc-pwm] 2 pwm_ctrl_reg_backup = %x pwm_ch0_period_backup = %x", pwm_ctrl_reg_backup, pwm_ch0_period_backup);
#endif

	device_init_wakeup(&pdev->dev, 1);

	/* register RTC and exit */
	rtc = rtc_device_register("rtc", &pdev->dev, &sunxi_rtcops, THIS_MODULE);
	if (IS_ERR(rtc)) {
		dev_err(&pdev->dev, "cannot attach rtc\n");
		ret = PTR_ERR(rtc);
		goto err_out;
	}
	ret = request_irq(sunxi_rtc_alarmno, sunxi_rtc_alarmirq,
			  IRQF_DISABLED,  "sunxi-rtc alarm", rtc);
	if (ret) {
		dev_err(&pdev->dev, "IRQ%d error %d\n", sunxi_rtc_alarmno, ret);
		rtc_device_unregister(rtc);
		return ret;
	}

	sw_rtc_dev = rtc;
	platform_set_drvdata(pdev, rtc);//?置rtc???据?pdev的私有?据

	return 0;

	err_out:
		return ret;
}

#ifdef CONFIG_PM
/* RTC Power management control
*  rtc do not to suspend, need to keep timing.
*/
#define sunxi_rtc_suspend NULL
#define sunxi_rtc_resume  NULL
#else
#define sunxi_rtc_suspend NULL
#define sunxi_rtc_resume  NULL
#endif

/*share the irq no. with timer2*/
static struct resource sunxi_rtc_resource[] = {
	[0] = {
		.start = SW_INT_IRQNO_ALARM,
		.end   = SW_INT_IRQNO_ALARM,
		.flags = IORESOURCE_IRQ,
	},
};

struct platform_device sunxi_device_rtc = {
	.name		    = "sunxi-rtc",
	.id		        = -1,
	.num_resources	= ARRAY_SIZE(sunxi_rtc_resource),
	.resource	    = sunxi_rtc_resource,
};


static struct platform_driver sunxi_rtc_driver = {
	.probe		= sunxi_rtc_probe,
	.remove		= __devexit_p(sunxi_rtc_remove),
	.suspend	= sunxi_rtc_suspend,
	.resume		= sunxi_rtc_resume,
	.driver		= {
		.name	= "sunxi-rtc",
		.owner	= THIS_MODULE,
	},
};

static struct i2c_board_info __initdata iris_rtc_i2c_board_info = {
		.type = "iris_m41t11",
		.addr = IRIS_RTC_M41T11_ADDR,
};

static int __init sunxi_rtc_init(void)
{
	int ret = 0;

	if ( main_rtc_selection == 0) {
		printk("# Rtc Setting : Default sunsi SOC rtc selection\n\n");
	} else {
		printk("# Rtc Setting : IRIS rtc (m41t11) selection -> IIS BusNum %d, addr 0x%X\n\n",i2c_bus ,iris_rtc_i2c_board_info.addr);
	}

	if ( main_rtc_selection == 0) {
		platform_driver_register(&sunxi_rtc_driver);
		return platform_device_register(&sunxi_device_rtc);
	} else {
		ret = i2c_register_board_info(i2c_bus, &iris_rtc_i2c_board_info, 1);
		return i2c_add_driver(&iris_m41t11_driver);
	}
}

static void __exit sunxi_rtc_exit(void)
{
	if ( main_rtc_selection == 0) {
		platform_driver_unregister(&sunxi_rtc_driver);
	} else {
		i2c_del_driver(&iris_m41t11_driver);
	}
}

ssize_t show_rtc_value(struct device *dev, struct device_attribute *attr, char *buf)
{
	if ( main_rtc_selection == 0 )
		return sprintf(buf, "Sunxi_rtc\n");
	else 
		return sprintf(buf, "m41t11_rtc\n");
}

ssize_t set_rtc_value(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	long val;
	int ret;

	if (strict_strtol(buf, 10, &val) != 0)
		return count;

	return count;
#if 0
	printk("# Rtc val %d, count %d, %s, %s\n",val, count , main_rtc_selection ? "m41t11_rtc" :  "Sunxi_rtc", (int)val ? "m41t11_rtc" :  "Sunxi_rtc");

	if ( main_rtc_selection != (int) val) {
		if ( main_rtc_selection == 0) {
			printk("#%d\n" , __LINE__);
				struct rtc_device *rtc = platform_get_drvdata(&sunxi_device_rtc);
				printk("#%d\n" , __LINE__);
			    rtc_device_unregister(rtc);
			printk("#%d\n" , __LINE__);
		} else {
			printk("#%d\n" , __LINE__);
			iris_m41t11_remove(gIris_m41t11->client);
			printk("#%d\n" , __LINE__);
		}
		printk("# Rtc exchange : %s -> %s\n" , main_rtc_selection ? "m41t11_rtc" :  "Sunxi_rtc", (int)val ? "m41t11_rtc" :  "Sunxi_rtc");
		main_rtc_selection = (int)val;

		if ( main_rtc_selection == 0) {
			printk("#%d\n" , __LINE__);
			sunxi_rtc_probe(&sunxi_device_rtc);
			printk("#%d\n" , __LINE__);
		} else {
			printk("#%d\n" , __LINE__);
			iris_m41t11_probe(gIris_m41t11->client, iris_m41t11_id);
		}
	}

	return count;
#endif
}

module_init(sunxi_rtc_init);
module_exit(sunxi_rtc_exit);

MODULE_DESCRIPTION("Sochip sunxi RTC Driver");
MODULE_AUTHOR("ben");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:sunxi-rtc");
