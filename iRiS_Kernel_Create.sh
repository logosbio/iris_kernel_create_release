#! /bin/bash
#


export unix_user="logosbio"
export user_home_directory=`pwd`

export install_directory=$user_home_directory"/install_result"
export COMPILE_DIR=$user_home_directory
export KERNEL_PATH=$user_home_directory"/linux-sunxi"

#	ALL				RELEASE ALL COMPILE

usage()
{
clear
cat << EOF


Logosbiosystem IRIS Kernel Dev Script New Ver 0.6.0 By rovhr hjyou@logosbio.com  
usage:options

OPTIONS:
	RELEASE_ALL			RELEASE  (CLONE & PATCH , COMPILE)
	KERNEL_CLONE			KERNEL CLONE (RETURN current_release Tag)
	KERNEL_PATCH			iRiS KERNEL PATCH
	KERNEL_REPAIR			ORIG KERNEL REPAIR
	DISTCLEAN			RELEASE ALL DISTCLEAN (ALL rm)
	clean				Clean kernel build
	k				Kernel Compile uImage & modules
	u				Kernel Compile uImage
	m				Kernel Compile modules
	INSTALL				Compile Result copy

	Next Ver Todo			1. Add etc
EOF
}


result_install()
{
	echo " " >&2
	echo "             INSTALL Play" >&2
	echo " " >&2

	if [ -f "./linux-sunxi/arch/arm/configs/logosiRiS_defconfig" ] ; then

		if [ ! -d "$install_directory" ];then
			echo Creating install directory $install_directory
			mkdir $install_directory
			mkdir $install_directory/rootfs
			mkdir $install_directory/BOOT
		fi

		cd $KERNEL_PATH
		make -j4 modules_install INSTALL_MOD_PATH=$install_directory/rootfs ; cp arch/arm/boot/uImage $install_directory/BOOT/; sync
		cd $COMPILE_DIR
		sync
	else
		echo "Not Kernel patch!!"
		echo "KERNEL_PATCH play!!"
	fi
}

clone_kernel()
{
	echo " " >&2
	echo "             KERNEL_CLONE Play" >&2
	echo " " >&2

	CURRENT_DIR=`pwd`
	echo "Sunxi main github kernel code clone!!!" >&2
	git clone https://github.com/linux-sunxi/linux-sunxi.git
	echo "kernel code dir move" >&2
	cd linux-sunxi
	echo "kernel sunxi-v3.4.103-r1 checkout" >&2
	git checkout sunxi-v3.4.103-r1
	cd $CURRENT_DIR
	echo "kernel code download End!!" >&2
	echo " " >&2
	echo " " >&2
}

kernel_patch()
{
	echo " " >&2
	echo "             KERNEL_PATCH Play" >&2
	echo " " >&2

	if [ -f "./linux-sunxi/arch/arm/configs/logosiRiS_defconfig" ] ; then
		echo "already patch !!"
		echo " " >&2
		exit 0
	fi

	if [ -d "./patch" ] ; then
		if [ -d "./linux-sunxi" ] ; then
		echo "orig file backup!!"

		mv ./linux-sunxi/arch/arm/tools/mach-types ./linux-sunxi/arch/arm/tools/mach-types_backup
		mv ./linux-sunxi/drivers/rtc/rtc-sun4i.c ./linux-sunxi/drivers/rtc/rtc-sun4i.c_backup
		mv ./linux-sunxi/drivers/rtc/rtc-sysfs.c ./linux-sunxi/drivers/rtc/rtc-sysfs.c_backup
		mv ./linux-sunxi/drivers/tty/vt/vt.c ./linux-sunxi/drivers/tty/vt/vt.c_backup
		mv ./linux-sunxi/drivers/video/logo/logo.c ./linux-sunxi/drivers/video/logo/logo.c_backup
		mv ./linux-sunxi/drivers/video/logo/logo_linux_clut224.ppm ./linux-sunxi/drivers/video/logo/logo_linux_clut224.ppm_backup

		echo "patch file copy!!"
		cp -Rf ./patch/arch/arm/tools/mach-types ./linux-sunxi/arch/arm/tools/mach-types
		cp -Rf ./patch/drivers/rtc/rtc-sun4i.c ./linux-sunxi/drivers/rtc/rtc-sun4i.c
		cp -Rf ./patch/drivers/rtc/rtc-sysfs.c ./linux-sunxi/drivers/rtc/rtc-sysfs.c
		cp -Rf ./patch/drivers/tty/vt/vt.c ./linux-sunxi/drivers/tty/vt/vt.c
		cp -Rf ./patch/drivers/video/logo/logo.c ./linux-sunxi/drivers/video/logo/logo.c
		cp -Rf ./patch/drivers/video/logo/logo_linux_clut224.ppm ./linux-sunxi/drivers/video/logo/logo_linux_clut224.ppm
		cp ./linux-sunxi/arch/arm/configs/sun7i_defconfig ./linux-sunxi/arch/arm/configs/logosiRiS_defconfig
		sync

		echo " " >&2
		echo "kernel logosiRiS_defconfig Setting !!"

		cd $KERNEL_PATH
		make -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- logosiRiS_defconfig

		echo "kernel config CONFIG_LOCALVERSION edit !!"
		sed -i 's/CONFIG_LOCALVERSION_AUTO=y/# CONFIG_LOCALVERSION_AUTO is not set/' .config
		sed -i 's/CONFIG_LOCALVERSION=""/CONFIG_LOCALVERSION="_logosbio"/' .config
		cd $COMPILE_DIR

		else
			echo "kernel file not find!!"
		fi
	else
		echo "patch file not find!!"
	fi
	echo " " >&2
}


kernel_repair()
{
	echo " " >&2
	echo "             KERNEL_REPAIR Play" >&2
	echo " " >&2

	if [ -f "./linux-sunxi/arch/arm/configs/logosiRiS_defconfig" ] ; then
		echo "patch file remove!!"
		rm -Rf ./linux-sunxi/arch/arm/tools/mach-types
		rm -Rf ./linux-sunxi/drivers/rtc/rtc-sun4i.c
		rm -Rf ./linux-sunxi/drivers/rtc/rtc-sysfs.c
		rm -Rf ./linux-sunxi/drivers/video/logo/logo.c
		rm -Rf ./linux-sunxi/drivers/video/logo/logo_linux_clut224.ppm
		rm -Rf ./linux-sunxi/arch/arm/configs/logosiRiS_defconfig
		rm -Rf ./linux-sunxi/drivers/tty/vt/vt.c

		echo "orig file repair!!"
		mv ./linux-sunxi/arch/arm/tools/mach-types_backup ./linux-sunxi/arch/arm/tools/mach-types
		mv ./linux-sunxi/drivers/rtc/rtc-sun4i.c_backup ./linux-sunxi/drivers/rtc/rtc-sun4i.c
		mv ./linux-sunxi/drivers/rtc/rtc-sysfs.c_backup ./linux-sunxi/drivers/rtc/rtc-sysfs.c
		mv ./linux-sunxi/drivers/tty/vt/vt.c_backup ./linux-sunxi/drivers/tty/vt/vt.c
		mv ./linux-sunxi/drivers/video/logo/logo.c_backup ./linux-sunxi/drivers/video/logo/logo.c
		mv ./linux-sunxi/drivers/video/logo/logo_linux_clut224.ppm_backup ./linux-sunxi/drivers/video/logo/logo_linux_clut224.ppm
	
		sync
	else
		echo "Not Kernel patch!!"
		echo "KERNEL_PATCH play!!"
	fi
}

kernel_compile()
{
	echo " " >&2
	echo "             KERNEL_COMPILE Play" >&2
	echo " " >&2

	if [ -f "./linux-sunxi/arch/arm/configs/logosiRiS_defconfig" ] ; then
		cd $KERNEL_PATH

		if [ "$KERNEL" == "1" ]; then
			make -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-  uImage modules
		fi

		if [ "$UIMAGE" == "1" ]; then
			make -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-  uImage
		fi

		if [ "$MODULES" == "1" ]; then
			make -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-  modules
		fi
		cd $COMPILE_DIR

	else
		echo "Not Kernel patch!!"
		echo "KERNEL_PATCH play!!"
		exit 0
	fi

}



RELEASE_ALL=
RELEASE=
CLEAN=
MISMATCH=
KERNEL_CLONE=
COMPILE_TARGET=
BUILD_THREADS=1
KERNEL=
UIMAGE=
MODULES=
PATCH=
REPAIR=
usage
RESULT=

while [ -n "$1" ]
do
	echo " " >&2
	echo " " >&2

 case $1 in
	ALL)
		ALL_COMPILE=1
		echo "$1: RELEASE ALL COMPILE" >&2
		;;

	RELEASE_ALL)
		RELEASE=1
		KERNEL=1
		echo "$1: RELEASE ALL COMPILE (ALL rm after clone & compile)" >&2
		;;

	DISTCLEAN)
		echo "$1: RELEASE ALL DISTCLEAN (ALL rm)" >&2
		rm -Rf linux-sunxi
		rm -Rf $install_directory
		;;

	KERNEL_CLONE)
		CLONE=1
		KERNEL=1;;

	KERNEL_PATCH)
		PATCH=1
		KERNEL=1;;

	KERNEL_REPAIR)
		REPAIR=1
		KERNEL=1;;

	ROOTFS_CREATE)
		ROOTFS=1;;

	k)
		KERNEL=1
		COMPILE_TARGET=1;;

	u)
		UIMAGE=1
		COMPILE_TARGET=1;;

	m)
		MODULES=1
		COMPILE_TARGET=1;;

	clean)
		KERNEL=1
		CLEAN=1;;

	INSTALL)
		echo "$1: Compile Result copy" >&2
		RESULT=1
		;;

	?)
		usage
		exit;;
 esac
 shift
done


if [ "$RELEASE" == "1" ]; then

	echo " " >&2
	echo "             DISTCLEAN Play" >&2
	echo " " >&2
	if [ ! -d "./linux-sunxi" ];then
		rm -Rf .linux-sunxi
	fi

	if [ ! -d "$install_directory" ];then
		rm -Rf $install_directory
		rm -Rf ./linux-sunxi
	fi

	clone_kernel
	kernel_patch
	kernel_compile
	result_install

	echo " " >&2
	echo "             RELEASE END" >&2
	echo " " >&2

	exit 0
fi


if [ "$COMPILE_TARGET" == "1" ]; then
	kernel_compile
	exit 0
fi

if [ "$CLONE" == "1" ]; then
	if [ "$KERNEL" == "1" ]; then
		clone_kernel
	fi
	exit 0
fi

if [ "$PATCH" == "1" ]; then
	if [ "$KERNEL" == "1" ]; then
		kernel_patch
	fi
	exit 0
fi

if [ "$REPAIR" == "1" ]; then
	if [ "$KERNEL" == "1" ]; then
		kernel_repair
	fi
	exit 0
fi



if [ "$CLEAN" == "1" ]; then
	if [ "$KERNEL" == "1" ]; then
		echo " " >&2
		echo "             KERNEL CLEAN Play" >&2
		echo " " >&2

		cd $KERNEL_PATH
		make -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- clean
		cd $COMPILE_DIR
	fi
	exit 0

fi


if [ "$RESULT" == "1" ]; then

	result_install
fi



